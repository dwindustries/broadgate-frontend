export { default as wrapRootElement } from "./src/state/ReduxWrapper"

// Hook into func that runs when the
// Gatsby browser runtime first starts
export const onClientEntry = () => {
  document.body.classList.add("initial-site-render")
}
