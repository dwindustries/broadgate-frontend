module.exports = {
  siteMetadata: {
    title: `Broadgate`,
    description: `The potfolio site of creative agency Broadgate.`,
    author: `https://dan-webb.com`,
    wordPressUrl: `https://broadgate-staging.co.uk`,
  },
  plugins: [
    {
      resolve: `gatsby-source-wordpress`,
      options: {
        /*
         * The full URL of the WordPress site's GraphQL API.
         */
        url: `https://broadgate-staging.co.uk/graphql`,
      },
    },
    `gatsby-plugin-netlify`,
    `gatsby-plugin-sass`,
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-js-fallback`,
    `gatsby-plugin-transition-link`,
    `gatsby-plugin-smoothscroll`,
    `gatsby-transformer-sharp`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    {
      resolve: `gatsby-plugin-sharp`,
      options: {
        defaultQuality: 100, // Make sure gatsby doesn't compress images (as we optimise manually)
      },
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `broadgate`,
        short_name: `broadgate`,
        start_url: `/`,
        background_color: `#252835`,
        theme_color: `#252835`,
        display: `minimal-ui`,
        icon: `src/images/favicon.svg`, // This path is relative to the root of the site.
      },
    },
    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        // The property ID; the tracking code won't be generated without it
        trackingId: `UA-19577042-3`,
        // Defines where to place the tracking script - `true` in the head and `false` in the body
        head: false,
      },
    },
    {
      resolve: `gatsby-plugin-google-tagmanager`,
      options: {
        id: `G-HQMD38TQ20`,

        // Name of the event that is triggered
        // on every Gatsby route change.
        routeChangeEventName: `ROUTE_CHANGE`,
      },
    },
  ],
}
