const path = require(`path`)
module.exports = async ({ actions, graphql }) => {
  const GET_USERS = `
  query GET_USERS($limit: Int, $skip: Int) {
    allWpUser(limit: $limit, skip: $skip) {
      pageInfo {
        hasNextPage
        itemCount
      }
      nodes {
        id
        slug
      }
    }
  }
  `
  const { createPage } = actions
  const allUsers = []
  const fetchUsers = async variables =>
    await graphql(GET_USERS, variables).then(({ data }) => {
      const {
        allWpUser: {
          nodes,
          pageInfo: { hasNextPage, itemCount },
        },
      } = data
      nodes.map(user => {
        allUsers.push(user)
      })
      if (hasNextPage) {
        return fetchUsers({ limit: variables.limit, skip: itemCount })
      }
      return allUsers
    })

  await fetchUsers({ limit: 100, skip: null }).then(allUsers => {
    const userTemplate = path.resolve(`./src/templates/user.js`)

    allUsers.map(user => {
      console.log(`create user: ${user.slug}`)
      createPage({
        path: `/author/${user.slug}`,
        component: userTemplate,
        context: user,
      })
    })
  })
}
