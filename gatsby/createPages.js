const path = require(`path`)
module.exports = async ({ actions, graphql }) => {
  const GET_PAGES = `
  query GET_PAGES($limit:Int, $skip:Int){
      allWpPage(
        limit: $limit 
        skip: $skip     
      ) {
        pageInfo {
          itemCount
          hasNextPage
        }
        nodes {
          id
          uri
          title
          pageTemplate
        }
      }
    }
  `
  const { createPage } = actions
  const allPages = []
  const fetchPages = async variables =>
    await graphql(GET_PAGES, variables).then(({ data }) => {
      const {
        allWpPage: {
          pageInfo: { hasNextPage, itemCount },
          nodes
        },
      } = data
      nodes.map(page => {
        allPages.push(page)
      })
      if (hasNextPage) {
        return fetchPages({ limit: variables.limit, skip: itemCount })
      }
      return allPages
    })

  await fetchPages({ limit: 100, skip: null }).then(allPages => {
    const templates = {
      default: path.resolve(`./src/templates/page.js`),
      home: path.resolve(`./src/templates/home.js`),
      about: path.resolve(`./src/templates/about.js`),
      solutions: path.resolve(`./src/templates/solutions.js`),
      contact: path.resolve(`./src/templates/contact.js`),
      child: path.resolve(`./src/templates/child.js`)
    }
    
    let pageTemplate = templates.default

    allPages.map(page => {
      switch (page.pageTemplate) {
        case 'templates/home.php':
          pageTemplate = templates.home          
          break;
        case 'templates/about.php':
          pageTemplate = templates.about          
          break;
        case 'templates/solutions.php':
          pageTemplate = templates.solutions          
          break;
        case 'templates/contact.php':
          pageTemplate = templates.contact          
          break;
        case 'templates/child.php':
          pageTemplate = templates.child
          break;
        default:
            pageTemplate = templates.default          
          break;
      }

      console.log(`create page: ${page.uri}`)
      
      // Now create all pages except the work page 
      // that we'll use for 'projects' index page (see createProjects.js)
      if (page.uri !== 'work') {
        console.log(page.uri)
        createPage({
          path: page.uri,
          component: pageTemplate,
          context: page,
        })
      }
    })
  })
}
