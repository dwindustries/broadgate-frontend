const path = require(`path`)
module.exports = async ({ actions, graphql }) => {
  const GET_PROJECTS = `
  query GET_PROJECTS($limit:Int $skip:Int){
    allWpProject(
      limit: $limit 
      skip: $skip
    ) {
      pageInfo {
        currentPage
        hasNextPage
        itemCount
      }
      nodes {
        id
        slug
        title
      }
    }
  }
  `
  const { createPage } = actions
  const allProjects = []
  const projectPages = []
  let pageNumber = 0
  const fetchProjects = async variables =>
    await graphql(GET_PROJECTS, variables).then(({ data }) => {
      const {
        allWpProject: {
          nodes,
          pageInfo: { currentPage, hasNextPage, itemCount },
        },
      } = data

      const nodeIds = nodes.map(node => node.id)
      const projectTemplate = path.resolve(`./src/templates/projects.js`)
      const projectPagePath = pageNumber === 0 ? `/work` : `/work/${pageNumber}`

      projectPages[pageNumber] = {
        path: projectPagePath,
        component: projectTemplate,
        context: {
          ids: nodeIds,
          pageNumber: pageNumber,
          hasNextPage: hasNextPage,
        },
        ids: nodeIds,
      }
      nodes.map(project => {
        allProjects.push(project)
      })
      if (hasNextPage) {
        pageNumber++
        return fetchProjects({ limit: 12, skip: itemCount })
      }
      return allProjects
    })

  await fetchProjects({ limit: 100, skip: null }).then(allProjects => {
    const projectTemplate = path.resolve(`./src/templates/project.js`)

    projectPages.map(projectPage => {
      console.log(`createProjectPage ${projectPage.context.pageNumber}`)
      createPage(projectPage)
    })

    allProjects.map(project => {
      console.log(`create project: ${project.slug}`)
      createPage({
        path: `/work/${project.slug}/`,
        component: projectTemplate,
        context: project,
      })
    })
  })
}
