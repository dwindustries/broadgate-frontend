const path = require(`path`)
module.exports = async ({ actions, graphql }) => {
  const GET_CATEGORIES = `
  query GET_CATEGORIES($limit: Int, $skip: Int) {
    allWpCategory(limit: $limit, skip: $skip) {
      pageInfo {
        hasNextPage
        itemCount
      }
      nodes {
        id
        slug
      }
    }
  }
  `
  const { createPage } = actions
  const allTags = []
  const fetchTags = async variables =>
    await graphql(GET_CATEGORIES, variables).then(({ data }) => {
      const {
        allWpCategory: {
          nodes,
          pageInfo: { hasNextPage, itemCount },
        },
      } = data
      nodes.map(category => {
        allTags.push(category)
      })
      if (hasNextPage) {
        return fetchTags({ limit: variables.limit, skip: itemCount })
      }
      return allTags
    })

  await fetchTags({ limit: 100, skip: null }).then(allTags => {
    const categoryTemplate = path.resolve(`./src/templates/category.js`)

    allTags.map(category => {
      console.log(`create category: ${category.slug}`)
      createPage({
        path: `/blog/category/${category.slug}`,
        component: categoryTemplate,
        context: category,
      })
    })
  })
}
