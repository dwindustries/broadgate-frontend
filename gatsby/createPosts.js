const path = require(`path`)
module.exports = async ({ actions, graphql }) => {
  const GET_POSTS = `
  query GET_POSTS($limit:Int, $skip:Int){
      allWpPost(
        limit: $limit 
        skip: $skip
      ) {
        pageInfo {
          itemCount
          currentPage
          hasNextPage
        }
        nodes {
          id
          slug
          title
        }
      }
    }
  `
  const { createPage } = actions
  const allPosts = []
  const blogPages = []
  let pageNumber = 0
  const fetchPosts = async variables =>
    await graphql(GET_POSTS, variables).then(({ data }) => {
      const {
        allWpPost: {
          nodes,
          pageInfo: { currentPage, hasNextPage, itemCount },
        },
      } = data

      const nodeIds = nodes.map(node => node.id)
      const blogTemplate = path.resolve(`./src/templates/blog.js`)
      const blogPagePath = pageNumber === 0 ? `/blog` : `/blog/${pageNumber}`

      blogPages[pageNumber] = {
        path: blogPagePath,
        component: blogTemplate,
        context: {
          ids: nodeIds,
          pageNumber: pageNumber,
          hasNextPage: hasNextPage,
        },
        ids: nodeIds,
      }
      nodes.map(post => {
        allPosts.push(post)
      })
      if (hasNextPage) {
        pageNumber++
        return fetchPosts({ limit: 6, skip: itemCount })
      }
      return allPosts
    })

  await fetchPosts({ limit: 100, itemCount: null }).then(allPosts => {
    const postTemplate = path.resolve(`./src/templates/post.js`)

    blogPages.map(blogPage => {
      console.log(`createBlogPage ${blogPage.context.pageNumber}`)
      createPage(blogPage)
    })

    allPosts.map(post => {
      console.log(`create post: ${post.slug}`)
      createPage({
        path: `/blog/${post.slug}/`,
        component: postTemplate,
        context: post,
      })
    })
  })
}
