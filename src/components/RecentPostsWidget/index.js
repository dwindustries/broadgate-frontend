import React from "react"
import { StaticQuery, graphql, Link } from "gatsby"
import { createLocalLink } from "../../utils/createLocalLink"

const QUERY = graphql`
  {
    allWpPost(limit: 5) {
      edges {
        node {
          id
          title
          link
        }
      }
    }
  }
`

const RecentPostsWidget = () => (
  <StaticQuery
    query={QUERY}
    render={data => {
      return (
        <div>
          <h2>Recent Posts</h2>
          <ul>
            {data && data.allWpPosts ? data.allWpPosts.edges.map(post => {
              return (
                <li key={post.node.link}>
                  <Link to={createLocalLink(post.node.link)}>{post.node.title}</Link>
                </li>
              )
            }) : null}
          </ul>
        </div>
      )
    }}
  />
)

export default RecentPostsWidget
