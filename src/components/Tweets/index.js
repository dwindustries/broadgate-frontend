import React from 'react'
import { useStaticQuery, graphql } from 'gatsby'

import Chevron from "../Symbols/chevron"

const Tweets = () => {
  // const data = useStaticQuery(graphql`
  //   query GET_TWEETS {
  //     twitterStatusesUserTimelineBroadgate {
  //       full_text
  //     }  
  //   }
  // `)  

  return <div className="card card--footer card--light-mode">
    <header className="card__header card__header--link">
      <a
        href="https://twitter.com/BroadgateTweets"
        data-cursor="alt"
        target="_blank"
        rel="noopener noreferrer"
      >
        Twitter <Chevron />
      </a>
    </header>
    <div className="card__body card__body--small">
      <div className="card__col card__col--contact-details">
        <p dangerouslySetInnerHTML={{__html: tweetFormat('')}} />
      </div>
    </div>
  </div>  
}

const tweetFormat = tweet => {
  let fullText = tweet.split(' ')

  // loop words and apply links to those relevant words
  const tweetWithLinks = fullText.map(word => {
    let url
    if(word.charAt(0) === '@') {
      url = `https://twitter.com/${word.replace('@','')}`
      word = `<a href="${url}" target="_blank">${word}</a>`
    } else if(word.charAt(0) === '#') {
      url = `https://twitter.com/search?q=${word.replace('#','')}`
      word = `<a href="${url}" target="_blank">${word}</a>`
    } else if(word.indexOf('https') > -1){
      url = `https://${word.split('https://').pop()}`
      word = `<a href="${url}" target="_blank">${word}</a>`
    }
    return word
  })

  return tweetWithLinks.join(' ')
}

export default Tweets