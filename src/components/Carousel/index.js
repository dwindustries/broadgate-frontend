import React from "react"
import Swiper from "react-id-swiper"

const Carousel = props => {
  // Swiper carousel parameters
  const carouselParams = {
    pagination: {
      el: '.carousel__pager',
      clickable: true
    },
    spaceBetween: 30,
    effect: 'fade',
  }

  const carouselClasses = (props.addSpace) ? `carousel carousel--space` : `carousel` 

  return (
    <div className={carouselClasses}>
      <Swiper {...carouselParams}>
        {props.children}
      </Swiper>
    </div>
  )
}

export default Carousel