import React from "react"
import { StaticQuery, graphql, Link } from "gatsby"

const QUERY = graphql`
  {
    allWpCategory {
      edges {
        node {
          name
          slug
          link
        }
      }
    }
  }
`

const CategoriesWidget = () => (
  <StaticQuery
    query={QUERY}
    render={data => {
      return (
        <div>
          <h2>Categories</h2>
          <ul>
            {data && data.allWpCategory ? data.allWpCategory.edges.map(category => {
              return (
                <li key={category.node.slug}>
                  <Link to={`/blog/category/${category.node.slug}`}>{category.node.name}</Link>
                </li>
              )
            }) : null}
          </ul>
        </div>
      )
    }}
  />
)

export default CategoriesWidget
