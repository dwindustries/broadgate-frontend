import React from 'react'
import moment from "moment/moment"
import { Link } from "gatsby"

const PostEntryMeta = ({ post }) => (
  <div>
    <Link to={`/author/${post.author.slug}`}>
    </Link>
      <Link to={`/author/${post.author.slug}`}>{post.author.name}</Link>
      <br />
      {moment(post.date).format(`MMM Do YY`)}
  </div>
)

export default PostEntryMeta
