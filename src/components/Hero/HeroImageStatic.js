import React from "react"

const HeroImageStatic = props => <img className={`hero__img-static ${props.solution ? `hero__img-static--solution` : `hero__img-static--about`}`} src={props.img} alt="Image" />

export default HeroImageStatic