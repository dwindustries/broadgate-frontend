import React from "react"

const ImageGroup = props => (
  <div className="image-group">
    {props.children}
  </div>
)

export default ImageGroup