import React from "react";

const ChevronDown = () => (
  <svg className="icon icon-chevron-down" width="36px" height="20px" viewBox="0 0 36 18">
    <path d="M35.3,0.4l-17,17c-0.3,0.3-0.7,0.3-1,0l-17-17"/>
  </svg>
)

export default ChevronDown