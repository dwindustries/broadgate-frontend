import React from "react";

const Chevron = () => (
  <svg className="icon icon-chevron" width="20px" height="36px" viewBox="0 0 18.2 35.9">
    <path d="M0.5,0.5l17,17c0.3,0.3,0.3,0.7,0,1l-17,17" />
  </svg>
)

export default Chevron