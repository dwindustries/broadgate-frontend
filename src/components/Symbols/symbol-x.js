import React from 'react'

const SymbolX = () => <svg className="icon icon-x" viewBox="0 0 35.43 35.63">
  <path d="M.35.35l17,17a.7.7,0,0,1,0,1l-17,17"/>
  <path d="M35.07,35.27l-17-17a.72.72,0,0,1,0-1l17-17"/>
</svg>

export default SymbolX