/* 
 * 
 * $Slider
 *
**/

.slider {
  position: relative;
  float: left;
  width: 100%;
  
  .swiper-container {
    overflow: visible;
  }
}

.slider--controls {
  padding-bottom: $ms4;
  
  .slider__controls {
    display: flex; // turn on controls if > 1 item
  }
  
  @include breakpoint(bp3){
    padding-bottom: $ms2;
  }
}

.slider--dark {
  .slider__next,
  .slider__prev {
    svg path { stroke: $brand2; }
    &:hover { path { stroke:$brand3; } }
    &.swiper-button-disabled:hover { path { stroke:$brand2; } }
  }  

  .slider__controls .swiper-pagination-bullet { border-color: $brand2; }
  .slider__controls .swiper-pagination-bullet-active { background-color: $brand2; }  
}


.slide {
  float: left;
  width: 100%;
  background-color: $brand1;
  @include border-radius($curve);
  overflow: hidden;
}


.slide__image {
  display: block;
  width: 100%;
  height: auto;
}


// Controls wrapper
.slider__controls {
  position: absolute;
  display: none;
  width:100%;
  justify-content: center;
  align-items: center;
  left:50%;
  bottom:-$ms-6;
  transform: translateX(-50%);

  @include breakpoint(bp3){
    bottom:-$ms0;
  }
}


// Navigation
.slider__next,
.slider__prev {
  display: block;
  margin:0 $ms-1;
  width: 15px;
  height: 30px;
  outline: none;
  cursor: pointer;

  @include breakpoint(bp5){
    &:hover {
      path { stroke:$hover; }
    }

    &.swiper-button-disabled:hover {
      cursor: default;
      path { stroke:$brand1; }
    }
  }

  svg {
    display: block;
    width: 100%;
    height: auto;
    
    path {
      fill:none;
      stroke:$brand1;
      stroke-linecap:round;
      stroke-miterlimit:10;
      stroke-width: 3;
      @include transition();
    }
  }

  &.swiper-button-disabled {
    opacity: 0.5;
  }
}
.slider__prev { left: 0%; }
.slider__next { right: 0%; }


// Pagination
.slider__pager {
  position: relative;
  display: block;
}


// Swiper Overwrites
.slider .swiper-pagination-bullet {
  width: 12px;
  height: 12px;
  margin:0 $ms-4; 
  border:2px solid $brand1;
  background-color: transparent;
  opacity: 1;
  outline: none;
  cursor: pointer;
  @include transition();

  @include breakpoint(bp3){
    margin:0 $ms-3; 

    &:hover {
      background-color: $brand1;
    }
  }
}

.slider .swiper-container-horizontal > .swiper-pagination-bullets .swiper-pagination-bullet {
  margin:0 $ms-4;

  @include breakpoint(bp3){
    margin:0 $ms-2 - 0.025em; 

    &:first-of-type { margin-left: 0; }
    &:last-of-type { margin-right: 0; }
  }  
}

.slider .swiper-pagination-bullet-active {
  background-color: $brand1;
}

